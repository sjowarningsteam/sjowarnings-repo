# SJOWARNINGS Project #
A project by Daniel Catalán, Jorge Cortés and Vitaly Sastre for the IAW Subject.

# README #

This README documents necessary steps in order to get the SJOWarning application up and running.

### What is this repository for? ###

* Quick summary
* Version

## Quick summary
This web application is a management tool that allows teachers to control the bad attitude of their students in the school.
Choosing the students in their classes, teachers can put warnings to students who have had bad behavior, depending on the seriousness of the situation. If the student of bad behavior has a specified number of warnings, the application will notify to the parents through telephone or email. If a student gets a lot of these warnings, he/she could be disciplined in a severe way, like academic ejection or reporting to social services, warning before to the parents.

## Version 
* Application: 1.0
* Database: 0.2.7

### How do I get set up? ###
For get this proyect set up and running you should 

1. Put the public_html folder in the Apache web server, o any path. For example, in the root of the web server filesystem ( /public_html ).
2. Get the file /Code/SQL/sjowarnings.sql
3. Depending of the permissions you have over your MySQL database Server/phpmyadmin:
  1. If you can import a database creation file: import the full file as a full database (structure+data).
  2. If you can import a database creation file but you can create a database across cpanel: 
    1. Create a new database (for example: sjowarnings).
    2. Open the /Code/SQL/sjowarnings.sql file and comment the lines 23 and 24. This lines create the database.
    3. In phpmyadmin, in this new database, import the file /Code/SQL/sjowarnings.sql. This will create all the tables structure and insert the existent data.
4. Open the file located at /Code/public_html/requires/require_mysqli_connect.php and make the next modifications in the database access information:
    1. DEFINE ('DB_USER', 'root'). Change username 'root' by the username of the database in the server ('root' by default).
    2. DEFINE ('DB_PASSWORD', 'root'). Change password by the password of the user in the database in the server ('root' by default). 
    3. DEFINE ('DB_HOST', 'localhost'). If necessary, change the host url of the database server ('localhost' by default).
    4. DEFINE ('DB_NAME', 'sjowarnings'). If necesary, change the database name for the database name in the server ('sjowarnings' by default).
5. In a web browser, open the url (following the example of path for the public_html folder) "server_ip:80/public_html/index.php". This will get you into the login page.
6. As speciffied in the Functional Requirements document, there are 3 types of users: students, teachers and headmasters. In order to test every user type, you can use the nexts user accounts (username / password):
  
    - As teacher: jual01 / jual01
  
    - As headmaster: daeg01 / daeg01

    - As student: maes01 / maes01


