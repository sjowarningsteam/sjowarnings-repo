create database sjowarnings_0_2_1_2;

	create table user_type(
		id_type int AUTO_INCREMENT not null,
		name_type varchar(20) not null,
		PRIMARY KEY (id_type));
	create table user(
		id_user int AUTO_INCREMENT not null,
		username varchar(15) not null,
		password varchar(40) not null,
		id_type int not null,
		PRIMARY KEY (id_user),
		FOREIGN KEY (id_type) references user_type(id_type));
create table teacher(
	id_teacher int AUTO_INCREMENT not null,
	name_teacher varchar(30) not null,
	lastname_teacher varchar(50) not null,
	phone_teacher varchar(15) not null,
	email_teacher varchar(50) not null,
	id_user int not null,
	PRIMARY KEY (id_teacher),
	FOREIGN KEY (id_user) references user(id_user));

create table student(id_student int AUTO_INCREMENT not null,
	name_student varchar(30) not null,
	lastname_student varchar(50) not null,
	phone_student varchar(15) not null,
	email_student varchar(50) not null,
	-- id_user int not null,
	PRIMARY KEY (id_student),
	-- FOREIGN KEY (id_user) references user(id_user));

create table headmaster(id_headmaster int AUTO_INCREMENT not null,
	name_headmaster varchar(30) not null,
	lastname_headmaster varchar(50) not null,
	phone_headmaster varchar(15) not null,
	email_headmaster varchar(50) not null,
	id_user int not null,
	PRIMARY KEY (id_headmaster),
	FOREIGN KEY (id_user) references user(id_user));

create table warning(
	id_warning int AUTO_INCREMENT not null,
	date_warning datetime not null,
	description varchar(140) not null,
	id_student int not null,
	id_teacher int not null,
	id_headmaster int not null,
	id_warning_status int not null,
	primary key (id_warning),
	foreign key (id_warning_status) references warning_status(id_warning_status),
	foreign key (id_student) references student(id_student),
	foreign key (id_teacher) references teacher(id_teacher),
	foreign key (id_headmaster) references headmaster(id_headmaster),

);

create table warning_status(
	id_warning_status int AUTO_INCREMENT not null,
	name_warning_status varchar(15) not null,
	primary key (id_warning_status));
