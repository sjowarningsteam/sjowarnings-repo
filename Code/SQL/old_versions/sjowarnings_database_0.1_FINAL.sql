-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8880
-- Generation Time: Jan 14, 2015 at 09:41 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sjowarnings`
--
CREATE DATABASE IF NOT EXISTS `sjowarnings` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sjowarnings`;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--
-- Creation: Jan 14, 2015 at 09:31 PM
--

CREATE TABLE IF NOT EXISTS `student` (
  `id_student` int(11) NOT NULL AUTO_INCREMENT,
  `name_student` varchar(20) NOT NULL,
  `lastname_student` varchar(50) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `photo_student` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_student`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--
-- Creation: Jan 14, 2015 at 09:31 PM
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `id_teacher` int(11) NOT NULL AUTO_INCREMENT,
  `name_teacher` varchar(20) NOT NULL,
  `lastname_teacher` varchar(50) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `id_ul` int(11) NOT NULL,
  PRIMARY KEY (`id_teacher`),
  KEY `id_ul` (`id_ul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--
-- Creation: Jan 14, 2015 at 09:31 PM
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `id_ul` int(11) NOT NULL AUTO_INCREMENT,
  `name_ul` varchar(10) NOT NULL,
  `passd_ul` varchar(43) NOT NULL,
  PRIMARY KEY (`id_ul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `warning`
--
-- Creation: Jan 14, 2015 at 09:33 PM
--

CREATE TABLE IF NOT EXISTS `warning` (
  `id_warning` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `date_warning` datetime NOT NULL,
  `description` varchar(140) DEFAULT NULL,
  `id_teacher` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  PRIMARY KEY (`id_warning`),
  KEY `warning_ibfk_1` (`id_teacher`),
  KEY `warning_ibfk_2` (`id_student`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`id_ul`) REFERENCES `user_login` (`id_ul`);

--
-- Constraints for table `warning`
--
ALTER TABLE `warning`
  ADD CONSTRAINT `warning_ibfk_2` FOREIGN KEY (`id_student`) REFERENCES `student` (`id_student`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `warning_ibfk_1` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
