-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8880
-- Generation Time: Jan 16, 2015 at 09:32 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sjowarnings_0_2_1_2`
--
CREATE DATABASE IF NOT EXISTS `sjowarnings_0_2_1_2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sjowarnings_0_2_1_2`;

-- --------------------------------------------------------

--
-- Table structure for table `headmaster`
--
-- Creation: Jan 15, 2015 at 08:29 PM
--

DROP TABLE IF EXISTS `headmaster`;
CREATE TABLE IF NOT EXISTS `headmaster` (
  `id_headmaster` int(11) NOT NULL AUTO_INCREMENT,
  `name_headmaster` varchar(30) NOT NULL,
  `lastname_headmaster` varchar(50) NOT NULL,
  `phone_headmaster` varchar(15) NOT NULL,
  `email_headmaster` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_headmaster`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--
-- Creation: Jan 16, 2015 at 09:31 AM
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id_student` int(11) NOT NULL AUTO_INCREMENT,
  `name_student` varchar(30) NOT NULL,
  `lastname_student` varchar(50) NOT NULL,
  `phone_student` varchar(15) NOT NULL,
  `email_student` varchar(50) NOT NULL,
  PRIMARY KEY (`id_student`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--
-- Creation: Jan 15, 2015 at 08:29 PM
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE IF NOT EXISTS `teacher` (
  `id_teacher` int(11) NOT NULL AUTO_INCREMENT,
  `name_teacher` varchar(30) NOT NULL,
  `lastname_teacher` varchar(50) NOT NULL,
  `phone_teacher` varchar(15) NOT NULL,
  `email_teacher` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_teacher`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--
-- Creation: Jan 15, 2015 at 08:29 PM
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(40) NOT NULL,
  `id_type` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_type` (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--
-- Creation: Jan 15, 2015 at 08:29 PM
--

DROP TABLE IF EXISTS `user_type`;
CREATE TABLE IF NOT EXISTS `user_type` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `name_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `warning`
--
-- Creation: Jan 16, 2015 at 08:41 AM
--

DROP TABLE IF EXISTS `warning`;
CREATE TABLE IF NOT EXISTS `warning` (
  `id_warning` int(11) NOT NULL AUTO_INCREMENT,
  `date_warning` datetime NOT NULL,
  `description` varchar(140) NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_teacher` int(11) NOT NULL,
  `id_headmaster` int(11) NOT NULL,
  `id_warning_status` int(11) NOT NULL,
  PRIMARY KEY (`id_warning`),
  UNIQUE KEY `id_warning_status` (`id_warning_status`),
  KEY `id_student` (`id_student`),
  KEY `id_teacher` (`id_teacher`),
  KEY `id_headmaster` (`id_headmaster`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `warning_status`
--
-- Creation: Jan 15, 2015 at 08:29 PM
--

DROP TABLE IF EXISTS `warning_status`;
CREATE TABLE IF NOT EXISTS `warning_status` (
  `id_warning_status` int(11) NOT NULL AUTO_INCREMENT,
  `name_warning_status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_warning_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `headmaster`
--
ALTER TABLE `headmaster`
  ADD CONSTRAINT `headmaster_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `user_type` (`id_type`);

--
-- Constraints for table `warning`
--
ALTER TABLE `warning`
  ADD CONSTRAINT `warning_ibfk_4` FOREIGN KEY (`id_warning_status`) REFERENCES `warning_status` (`id_warning_status`),
  ADD CONSTRAINT `warning_ibfk_1` FOREIGN KEY (`id_student`) REFERENCES `student` (`id_student`),
  ADD CONSTRAINT `warning_ibfk_2` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`),
  ADD CONSTRAINT `warning_ibfk_3` FOREIGN KEY (`id_headmaster`) REFERENCES `headmaster` (`id_headmaster`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
