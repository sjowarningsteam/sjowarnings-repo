-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8880
-- Generation Time: Jan 16, 2015 at 09:53 AM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sjowarnings_0_2_1_2`
--

--
-- Dumping data for table `headmaster`
--

INSERT INTO `headmaster` (`id_headmaster`, `name_headmaster`, `lastname_headmaster`, `phone_headmaster`, `email_headmaster`, `id_user`) VALUES
(2, 'David', 'Egea Juan', '999888777', 'david.egea@sjowarning.com', 3);

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id_student`, `name_student`, `lastname_student`, `phone_student`, `email_student`) VALUES
(1, 'Daniel', 'Catalán Sánchez', '666666666', 'daniel.catalan@sjowarning.com'),
(2, 'Jorge', 'Cortés Anguita', '666666666', 'jorge.cortes@gmail.com'),
(3, 'Vitaly', 'Sastre Cuenca', '666666666', 'vitaly.sastre@gmail.com'),
(4, 'Hugo', 'Moragues Valencia', '666666666', 'hugo.moragues@sjowarning.com'),
(5, 'Luís', 'Martínez González', '666666669', 'luis.martinez@sjowarning.com');

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id_teacher`, `name_teacher`, `lastname_teacher`, `phone_teacher`, `email_teacher`, `id_user`) VALUES
(1, 'Nicolau', 'Miró Valls', '777777771', 'nico.miro@sjowarnings.com', 2),
(2, 'Juan Ramón', 'Alemany Hormaeche', '777777772', 'juanra.alemany@sjowarnings.com', 1);

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `id_type`) VALUES
(1, 'jual01', 'd906337d19abdae315ed0f1ee74f2bd0b77cd897', 2),
(2, 'nimi01', 'ae6b7e26b5946943da3141715de747bc0eac3c27', 2),
(3, 'daeg01', '5909eaa0e3b2cd7f1edf20b6e0c9acf2a1a52afe', 1);

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id_type`, `name_type`) VALUES
(1, 'Headmaster'),
(2, 'Teacher'),
(3, 'Student');

--
-- Dumping data for table `warning_status`
--

INSERT INTO `warning_status` (`id_warning_status`, `name_warning_status`) VALUES
(1, 'Pending'),
(2, 'Accepted'),
(3, 'Refused');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
