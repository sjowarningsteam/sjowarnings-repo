-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost:8880
-- Generation Time: Jan 29, 2015 at 12:51 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sjowarnings`
-- Version: `0.2.7`
--
CREATE DATABASE IF NOT EXISTS `sjowarnings` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sjowarnings`;

-- --------------------------------------------------------

--
-- Table structure for table `headmaster`
--

DROP TABLE IF EXISTS `headmaster`;
CREATE TABLE IF NOT EXISTS `headmaster` (
  `id_headmaster` int(11) NOT NULL AUTO_INCREMENT,
  `name_headmaster` varchar(30) NOT NULL,
  `lastname_headmaster` varchar(50) NOT NULL,
  `phone_headmaster` varchar(15) NOT NULL,
  `email_headmaster` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_headmaster`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `headmaster`
--

INSERT INTO `headmaster` (`id_headmaster`, `name_headmaster`, `lastname_headmaster`, `phone_headmaster`, `email_headmaster`, `id_user`) VALUES
(2, 'David', 'Egea Juan', '999888777', 'david.egea@sjowarning.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `id_student` int(11) NOT NULL AUTO_INCREMENT,
  `name_student` varchar(30) NOT NULL,
  `lastname_student` varchar(50) NOT NULL,
  `phone_student` varchar(15) NOT NULL,
  `email_student` varchar(50) NOT NULL,
  `photo_student` int(11) DEFAULT NULL,
  `points_quantity_student` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_student`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id_student`, `name_student`, `lastname_student`, `phone_student`, `email_student`, `photo_student`, `points_quantity_student`, `id_user`) VALUES
(1, 'Daniel', 'Catalán Sánchez', '666666666', 'daniel.catalan@sjowarning.com', 1, 0, 4),
(2, 'Jorge', 'Cortés Anguita', '666666666', 'jorge.cortes@gmail.com', 2, 0, 5),
(3, 'Vitaly', 'Sastre Cuenca', '666666666', 'vitaly.sastre@gmail.com', 3, 0, 6),
(4, 'Hugo', 'Moragues Valencia', '666666666', 'hugo.moragues@sjowarning.com', 4, 0, 7),
(5, 'Luís', 'Martínez González', '666666669', 'luis.martinez@sjowarning.com', 5, 0, 8),
(6, 'David', 'Alonso Cano', '123123123', 'david.alonso@sjo.com', NULL, 0, 9),
(7, 'David', 'García Torralvo', '321321321', 'david.garcia@sjo.net', NULL, 0, 10),
(8, 'Mateu', 'Escarrer Montserrat', '696969696', 'mateu.escarrer@hotmail.com', NULL, 0, 11);

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
CREATE TABLE IF NOT EXISTS `teacher` (
  `id_teacher` int(11) NOT NULL AUTO_INCREMENT,
  `name_teacher` varchar(30) NOT NULL,
  `lastname_teacher` varchar(50) NOT NULL,
  `phone_teacher` varchar(15) NOT NULL,
  `email_teacher` varchar(50) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_teacher`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id_teacher`, `name_teacher`, `lastname_teacher`, `phone_teacher`, `email_teacher`, `id_user`) VALUES
(1, 'Nicolau', 'Miró Valls', '777777771', 'nico.miro@sjowarnings.com', 2),
(2, 'Juan Ramón', 'Alemany Hormaeche', '777777772', 'juanra.alemany@sjowarnings.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(40) NOT NULL,
  `id_type` int(11) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_type` (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `id_type`) VALUES
(1, 'jual01', 'd906337d19abdae315ed0f1ee74f2bd0b77cd897', 2),
(2, 'nimi01', 'ae6b7e26b5946943da3141715de747bc0eac3c27', 2),
(3, 'daeg01', '5909eaa0e3b2cd7f1edf20b6e0c9acf2a1a52afe', 1),
(4, 'daca01', '66a1ee45f8c5a585bae06b2861ad3da538ac2ca7', 3),
(5, 'joco01', '32c8046001d5da322ba87025b556804f26f1d07c', 3),
(6, 'visa01', '589d4f72d0220b09104a7147cfc5c3d653ff21a7', 3),
(7, 'humo01', 'f49061a62f5be6b4c646fa9694a7e494ffbde027', 3),
(8, 'luma01', '02f73ad210a6167a10957e5f08ceaaeabd90cdc5', 3),
(9, 'daal01', 'f1df7c642fb89e046097cfeecd4500971d1dc731', 3),
(10, 'daga01', '2f314de40d746c292b027282decc855f44940725', 3),
(11, 'maes01', '5ea514b54c9354fffe5b30ad6ffdb85ada0cd40d', 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
CREATE TABLE IF NOT EXISTS `user_type` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `name_type` varchar(20) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id_type`, `name_type`) VALUES
(1, 'Headmaster'),
(2, 'Teacher'),
(3, 'Student');

-- --------------------------------------------------------

--
-- Table structure for table `warning`
--

DROP TABLE IF EXISTS `warning`;
CREATE TABLE IF NOT EXISTS `warning` (
  `id_warning` int(11) NOT NULL AUTO_INCREMENT,
  `date_warning` datetime NOT NULL,
  `description` varchar(590) NOT NULL,
  `points_quantity_warning` int(11) NOT NULL DEFAULT '0',
  `id_student` int(11) NOT NULL,
  `id_teacher` int(11) NOT NULL,
  `id_headmaster` int(11) NOT NULL,
  `id_warning_status` int(11) NOT NULL,
  PRIMARY KEY (`id_warning`),
  KEY `id_student` (`id_student`),
  KEY `id_teacher` (`id_teacher`),
  KEY `id_headmaster` (`id_headmaster`),
  KEY `id_warning_status` (`id_warning_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `warning`
--

INSERT INTO `warning` (`id_warning`, `date_warning`, `description`, `points_quantity_warning`, `id_student`, `id_teacher`, `id_headmaster`, `id_warning_status`) VALUES
(72, '2015-01-29 00:49:37', 'Breaking the law.', 4, 2, 2, 2, 2),
(73, '2015-01-29 00:49:53', 'Breaking the wall.', 15, 4, 2, 2, 3),
(74, '2015-01-29 00:50:06', 'Missing school uniform.', 3, 6, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `warning_status`
--

DROP TABLE IF EXISTS `warning_status`;
CREATE TABLE IF NOT EXISTS `warning_status` (
  `id_warning_status` int(11) NOT NULL AUTO_INCREMENT,
  `name_warning_status` varchar(15) NOT NULL,
  PRIMARY KEY (`id_warning_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `warning_status`
--

INSERT INTO `warning_status` (`id_warning_status`, `name_warning_status`) VALUES
(1, 'Pending'),
(2, 'Accepted'),
(3, 'Refused'),
(4, 'Expired');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `headmaster`
--
ALTER TABLE `headmaster`
  ADD CONSTRAINT `headmaster_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `teacher_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `user_type` (`id_type`);

--
-- Constraints for table `warning`
--
ALTER TABLE `warning`
  ADD CONSTRAINT `warning_ibfk_1` FOREIGN KEY (`id_student`) REFERENCES `student` (`id_student`),
  ADD CONSTRAINT `warning_ibfk_2` FOREIGN KEY (`id_teacher`) REFERENCES `teacher` (`id_teacher`),
  ADD CONSTRAINT `warning_ibfk_3` FOREIGN KEY (`id_headmaster`) REFERENCES `headmaster` (`id_headmaster`),
  ADD CONSTRAINT `warning_ibfk_4` FOREIGN KEY (`id_warning_status`) REFERENCES `warning_status` (`id_warning_status`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
