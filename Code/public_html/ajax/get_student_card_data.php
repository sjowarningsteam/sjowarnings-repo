<?php
// This file returns all the student data requested by the id_student given.

// Session stuff
// -----------------------------------
session_start();
// -----------------------------------

require '../requires/require_mysqli_connect.php';

// Check if the id_student is a integer value.
if (ctype_digit($_POST['id_student'])) {

    // Get user info from database
    $query = "SELECT concat_ws(', ',`lastname_student`, `name_student`) as student, `points_quantity_student`FROM  `student` WHERE `id_student`=".$_POST['id_student']."";

    // var_dump($query);
    $result = @mysqli_query($dbc,$query);

    // var_dump($result);
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
     echo json_encode($row);
 }
} else {
    echo "Error. Student_id not valid.";
}
?>