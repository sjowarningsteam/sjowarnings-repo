<?php
// This file gets all the data from $_SESSION variable and the data sent by the student-card form.

// Session stuff
session_start();

// Database connection
require '../requires/require_mysqli_connect.php';

// Check input data
// ------------------------------------------------------

// Check if there is content in the description
if ( !isset( $_POST['description'] ) && empty( $_POST['description']) ) {
    $errors = "Description is missing.";
}

// Check the description given and avoid an SQL injection attack
$_POST['description'] = htmlentities($_POST['description']);
$_POST['description'] = mysqli_real_escape_string($dbc, trim($_POST['description']));

// Check if these values have value and is numerical
if( !ctype_digit( $_POST['points'] ) 
   || !ctype_digit( $_POST['id_student'] ) 
   || !ctype_digit( $_SESSION['id_teacher'] )) {
    $errors[] = "Bad numerical input as points or some id.";
}
// ------------------------------------------------------

// If there is no errors, continue with the database connection
if(empty($errors) || !isset($errors)) {

    // Disable autocommit after every query.
    mysqli_autocommit($dbc, FALSE);

    // Flag variable in order to control if any query went bad.
    $all_query_ok = true;

    // Insert warning
    $query_insert = "INSERT INTO `warning`(`date_warning`, `description`, `points_quantity_warning`, `id_student`, `id_teacher`, `id_headmaster`, `id_warning_status`) VALUES (NOW(), '".$_POST['description']."',".$_POST['points'].",".$_POST['id_student'].",".$_SESSION['id_teacher'].",2,1)";
    mysqli_query($dbc, $query_insert);
    mysqli_affected_rows($dbc) ? null : $all_query_ok = false;

    // If all the querys went Ok, commit the transaction. In any other case, rollback.
    if($all_query_ok) {
        mysqli_commit($dbc);
        echo "Warning inserted correctly.";
    }
    else{ 
        mysqli_rollback($dbc);
        echo "Database error. Please try again later.";

    }
    // Close Database connection.
    mysqli_close($dbc);
}
?>