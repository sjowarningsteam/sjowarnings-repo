<?php 



    // Database connection
if($_SERVER['REQUEST_METHOD'] == "POST") {
    session_start();
    require '../requires/require_mysqli_connect.php';
}
$id_user = $_SESSION['id_user'];
    // Get user info from database
$query = "SELECT id_warning, DATE_FORMAT(date_warning,'%Y-%m-%d %H:%i'), description, points_quantity_warning, concat_ws(', ',LASTNAME_STUDENT, NAME_STUDENT) as Student, concat_ws(', ' ,lastname_teacher, name_teacher) as Teacher, 
concat_ws(', ',lastname_headmaster, Name_HEADMASTER) as Headmaster FROM warning as w inner join student as s on w.id_student = s.id_student inner join teacher as t on w.id_teacher = t.id_teacher inner join headmaster as h on w.id_headmaster = h.id_headmaster WHERE id_warning_status = 1";
$result = @mysqli_query($dbc,$query);
    // Table header.
echo '<table>';
echo '<tr><th id="th_date">Date</th><th>Description</th><th id="th_points">Points</th><th>Student</th><th>Teacher</th><th>Headmaster</th><th id="th_accepted">Accept/Refuse</th></tr>';
    // Fetch and print all the records:
while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
    echo '<tr id='.$row["id_warning"].'>';
    foreach ($row as $key => $value) {
        if($key != "id_warning"){
            echo "<td align='center'>".$value."</td>";
        }
    }
    echo '<td><center><button class="v">✓</button><button class="x">✕</button></center></td></tr>';
}
    // Close the table.
echo '</table>'; 

    // Free up the resources
mysqli_free_result ($result);
?>