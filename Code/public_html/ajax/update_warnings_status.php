<?php 
// This file updates the warnings status from the warning table and.
// Once the status changes to "accepted", update de quantity of points of the student..

// Session stuff
session_start();

// Database connection
require '../requires/require_mysqli_connect.php';

#########   FUNCTIONS  #########

// Given an array of values, return a string in the WHERE IN format of MySQL
// Example: (1, 2 ,3)
function buildUpdateInQuery($array){
    $buildString = "";
    foreach ($array as $key) {
        $buildString .= $key;

        // Check if the current element is the last element for insert a comma
        if($array[count($array)-1] !== $key) {
            $buildString .= ", ";
        }
    }
    return $buildString;
}

// Check if given variable has value
function checkEmptySet($var){
    return (isset($var) && !empty($var));
}

// Build and execute update query
function queryUpdate($array, $status, $dbc) {
    if (checkEmptySet($array)) {
        $data = buildUpdateInQuery($array);
        $query = "UPDATE warning set id_warning_status = ".(string)$status." WHERE id_warning IN (".$data.")";    
        echo "Query: ".$query;
        $result = @mysqli_query($dbc, $query);
        echo "affected rows: ".mysqli_affected_rows($dbc);
    } else {
        $errors[] = $array;
    }
}

// If there is no errors, continue with the database connection
if(empty($errors) || !isset($errors)) {

    // Disable autocommit after every query.
    mysqli_autocommit($dbc, FALSE);

    // Flag variable in order to control if any query went bad.
    $all_query_ok = true;

    // Update warnings status
    if (isset($_POST['accepted'])) {
        queryUpdate($_POST['accepted'],2,$dbc);
        mysqli_affected_rows($dbc) ? null : $all_query_ok = false;    
    }

    if (isset($_POST['refused'])) {
        queryUpdate($_POST['refused'],3,$dbc);
        mysqli_affected_rows($dbc) ? null : $all_query_ok = false;
    }

    // Once that the headmaster has accepted the warning, update the points quantity of the student
    // Update student' points
    $query_update = "UPDATE `student` set `points_quantity_student` = `points_quantity_student` + ".$_POST['points']." WHERE `id_student` =  ".$_POST['id_student']."";
    mysqli_query($dbc, $query_update);
    mysqli_affected_rows($dbc) ? null : $all_query_ok = false;

    // If all the querys went Ok, commit the transaction. In any other case, rollback.
    if($all_query_ok) {
        mysqli_commit($dbc);
        echo "Warning updated correctly.";
    }
    else{ 
        mysqli_rollback($dbc);
        echo "Database error. Please try again later.";

    }
    // Close Database connection.
    mysqli_close($dbc);
}
?>