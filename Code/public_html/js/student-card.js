// Create custom dialog instead of standard alert()
function createDialog(data){
	$("<div>"+data+"</div>").dialog({
		modal: true,
		closeOnEscape: false,
		dialogClass: "no-close",
		resizable: false,
		buttons: { Ok: function() {
			$( this ).dialog( "close" );
		}
	}
});
}

// Create custom confirm dialog instead of standard confirm()
function createConfirm(dialogToClose,data){
	isClose = false;
	$("<div>"+data+"</div>").dialog({
		resizable: false,
		height:140,
		modal: true,
		closeOnEscape: false,
		dialogClass: "no-close",
		resizable: false,
		buttons: {
			"Exit": function() {
				console.log($(this));
				$( this ).dialog( "close" );
				dialogToClose.dialog( "close" );
			},
			Cancel: function() {
				$( this ).dialog( "close" );
			}
		}
	});
}

// Get HTML elements
var points = document.getElementById('points');
var button_plus = document.getElementById('button_plus');
var button_minus = document.getElementById('button_minus');
var button_confirm = document.getElementById('button_confirm');
var button_cancel = document.getElementById('button_cancel');
var char_counter = document.getElementById('char_counter');
var descript_area = document.getElementById('description');

// Events functions
// ----------------

// Add points
button_plus.onclick = function(){

// Check if limit of points is reached.
if(points.value < 15){
	points.value++;
} else {
	createDialog("Currently, 15 points is the limit.");
}
};

// Substract points
button_minus.onclick = function(){

// Check if counter is 0. Points counter can't be negative. 
if(points.value > 0) {
	points.value--;
}
};

// Check characters counter and update it
descript_area.oninput=function(){
	char_counter.innerHTML = descript_area.value.length;
};