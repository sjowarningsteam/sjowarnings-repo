<!-- Author: 	Jorge Cortés -->
<!-- Date: 		150122 -->
<!-- File: 		require_template_top.php -->

<?php

// Session stuff
// -----------------------------------
session_start();
if (!isset($_SESSION['username'])) {
	header('location: index.php');
	exit();
}
// -----------------------------------
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $pageTitle; ?></title>
	<link rel="shortcut icon" href="img/favicon_pencil.png">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script src="js/jquery-ui-1.11.2/jquery.js" type="text/javascript"></script>
	<script src="js/jquery-ui-1.11.2/jquery-ui.js" type="text/javascript"></script>
	<link rel="stylesheet" type='text/css' href="js/jquery-ui-1.11.2/jquery-ui.css">
	<script type="text/javascript">

	// Start script when the full document will be ready.
	$(document).ready(function() {

		// Tooltip only Text
		$('#ukflag_logo').hover(function(){
		
		// Hover over code
		var title = $(this).attr('title');
		$(this).data('tipText', title).removeAttr('title');
		$('<p class="tooltip"></p>')
		.text(title)
		.appendTo('body')
		.fadeIn('slow');
	}, function() {
        
        // Hover out code
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
    }).mousemove(function(e) {
    	
    	//Get X coordinates
    	var mousex = e.pageX - 100;
        
        //Get Y coordinates 
        var mousey = e.pageY + 15; 
        $('.tooltip')
        .css({ top: mousey, left: mousex })
    });
});

</script>
</head>
<body>
	<div id="container">
		<header id="header">
			<div id="logo"><img src="img/logo-sjo-full.png"></div>
			<h1>SJOWARNINGS</h1>
			<div id="ukflag_logo" title="In English, please."></div>
			<div id="user_info">
				<?php
				// Database connection
				require 'requires/require_mysqli_connect.php';
				$table = strtolower($_SESSION['name_type']);
				// Get user info from database
				$query = "select name_".$table.", lastname_".$table." , id_".$table."  from ".$table." where id_user='".$_SESSION['id_user']."'";
				$result = @mysqli_query($dbc,$query);
				if($row = mysqli_fetch_array($result, MYSQL_ASSOC))
				{
					$_SESSION['id_'.$table.''] = $row['id_'.$table.''];
					$name = $row['name_'.$table.''];
					$lastname = $row['lastname_'.$table.''];
				}
				// Show logged user info
				echo '<ul><li>User: '.$name.' '. $lastname.'</li>';
				echo '<li>Logged as: '.$_SESSION['name_type'].'</li></ul>';
				?>
			</div>			
			<div id="div_logout">
				<a href="requires/require_session_destroy.php">Logout</a>
			</div>
			<?php 
				// Load menu
			require 'requires/require_menu_top.php'; 
			?>
		</header>
		<div id="content">
			<div id="centered">
				
				<div id="content-middle">