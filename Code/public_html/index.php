<!-- Author: 	Daniel Catalán -->
<!-- Date: 		20150118 -->
<!-- File: 		index.php -->

<?php 
// Start session
session_start();

// If there is a session started, destroy it
if(isset($_SESSION['username'])) require 'requires/require_session_destroy.php';

// Validate form
// ========================================================================
if($_SERVER['REQUEST_METHOD'] == 'POST')
	require 'requires/require_validate_user.php';
// ========================================================================

// Show cookies alert message
require 'requires/modules/cookies_warning.html';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="shortcut icon" href="img/favicon_pencil.png">
	<link rel="stylesheet" type="text/css" href="css/login.css">
	<meta charset="UTF-8">
	<title>Login</title>

	<!-- If any, show login errors -->
	<?php if (!empty($errors)) {
		?>
		<script>
			alert("<?php
				foreach($errors as $value){
					echo $value.'\n';
				}
				echo 'Please check and try again.';
				?>");
			</script>
			<?php
		} ?>

	</head>
	<body>
		<div id="container">
			<form action="" method="POST" class="login">
				<h1>SJOWARNINGS</h1>
				<div>
					<label>Username:</label>
					<input name="username" type="text" /></td>
				</div>
				<div>
					<label>Password:</label>
					<input name="password" type="password"</td>
				</div>
				<div>
					<input name="iniciar" class="submit" type="submit" value="Log In">
				</div>
			</form>
		</div>
	</body>
	</html>