<?php 
// This file gets student list from database and puts the result as an HTML table

$query = "SELECT concat_ws(', ',`lastname_student`, `name_student`) as student, `points_quantity_student`, `id_student` FROM  `student` ";
$result = @mysqli_query($dbc,$query);

// If any, print the results as a table.
if(mysqli_num_rows($result)){
    ?>
    <div id="table_students"><table id="tableStud">
        <thead>
            <tr class="centered">
                <th>Photo</th>
                <th class="sortable" data-sort="string">Student</th>
                <th class="sortable" data-sort="int">Points</th>
                <th>Warning</th>
            </tr>
        </thead>
        <tbody>
            <?php 

            // Fetch and print all the records:
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

                // As the project has no photos database, use the same image for every register
                echo '<tr><td><img class="id_photo" src="img/rabbit_avatar.png"></img></td>';
                
                // Show all the values that we want in the table.
                foreach ($row as $key => $value) {

                    // id_student will be used for insert warnings, later.
                    if($key != "id_student") {
                        echo "<td align='center'>".$value."</td>";
                    }
                }
                echo '<td><img id="'.$row["id_student"].'"class="edit-button" src="img/add-icon.png"></img></td></tr>';
            }
        }
            // Free up the resources
        mysqli_free_result ($result);
        ?>
        
        <!-- Close the table. -->
    </tbody>
</table>
</div>