<?php

// Session stuff
session_start();

//Empty session
$_SESSION = array();

//Destroy session
session_destroy();

//Destroy cookie
setcookie('PHPSESSID', '', time()-3600, '/','',0,0);

//redirect to login page
$login_file = "index.php";
if(file_exists("../".$login_file)) {
    $login_file = "../".$login_file;
} 
header("location: ".$login_file)
?>