<!-- Author: 	Jorge Cortés -->
<!-- Date: 		20150119 -->
<!-- File: 		student-list.php -->
<div id="students-list">
	<link rel='stylesheet' href='css/student-card.css'>
	<link rel="stylesheet" href="css/students-list.css">
  <!-- // Plugin for sort table -->
  <!-- // From: https://github.com/joequery/Stupid-Table-Plugin#stupid-jquery-table-sort -->
  <script src="js/stupidtable.js"></script>
  <script>

  // Execute script when document is ready
  $(document).ready(function() {

        // Make the table sortable
        $("#tableStud").stupidtable();

            // Hightlight row on hover mouse in a table
            $("tr").not(':first').hover(
                                        function () {
                                          $(this).css("background","#ffff66");
                                        }, 
                                        function () {
                                          $(this).css("background","");
                                        });

            // When any edit button (pencil) were clicked, show a dialog
            $('.edit-button').click(function(){
              var id_student_img = $(this).attr('id');
              var dialogo_id = "student-"+ id_student_img;

                // Dialog' div code
                var dialogo = "<div id='"+dialogo_id+"' class='student-card'><div id='sc-photo'><img src='img/rabbit_avatar.png'></div><div id='sc-personal-info'><ul><li>Name: <label id='label-student-name'></label></li><li>Current points: <label id='label-student-points'></label></li></ul><div id='set-points'><button id='button_plus'>+</button><input id='points' type='text' value='0' disabled><button id='button_minus'>-</button></div></div><div id='sc-description'><textarea placeholder='Description' id='description' maxlength='590'></textarea><p id='chars_written'>Characters written: <span id='char_counter'>0</span> of 590</p></div></div>";
                $(document.body).append(dialogo);

                // Load script referent to the student card (dialog) when this exists as DOM element
                $.getScript( "js/student-card.js");

                // Encapsulate data (student id) that is gonna be sent to the server
                var data_id = { "id_student": id_student_img };


                // Call to the server requesting student info
                $.ajax({
                 data: data_id,
                 dataType: "text",
                 type: "POST",
                 url: "ajax/get_student_card_data.php"
               })
               // In case of succesfull connection with the server
               .done(function(data) {
                
                console.log("ajax done: "+data);
                    // Get requested data as json and parse it as Javascript object.
                    data = $.parseJSON(data);

                    console.log("ajax done - object format: "+data);
                    var db_student = data["student"];
                    var db_points = data["points_quantity_student"];

                    // Put that info in the student card
                    $("#label-student-name").html(db_student);
                    $("#label-student-points").html(db_points);
                  })

                // In case of connection error, warn the user
                .error(function(){
                  createDialog("Database connection error.\nPlease try again later.");
                });

                // Create the dialog object with the dialog() JQuery UI function.
                $("#"+dialogo_id).dialog({

                    // Set properties for the dialog object.
                    height: 400,
                    modal: true,
                    width: 625,

                    // When the dialog object is closed, remove all its content from the DOM.
                    close: function() { $(this).remove(); },
                    closeOnEscape: false,
                    dialogClass: "no-close",
                    resizable: false,
                    show: { effect: "blind", duration: 300 },

                   // Set dialog object buttons
                   buttons: [
                   {
                    text: "Confirm",
                    id: "button_confirm",
                    click: function() {

                    // Checks different cases of warning input
                    if((points.value == 0) && (char_counter.innerHTML == 0)) {
                      createDialog("You must set at leats 1 point and write a description for this warning.");
                    } else if( points.value == 0) {
                      createDialog("You must set at leats 1 point in this warning.");
                    } else if(char_counter.innerHTML == 0) {
                      createDialog("You must write a description for this warning.");
                    } else {

                            // Database connection for insert warning
                            // ---------------------------------
                            var data_warning = { 
                              "id_student": id_student_img,
                              "points": points.value,
                              "description": descript_area.value
                            };

                            // AJAX call to the server for insert warning info
                            $.ajax({
                             data: data_warning,
                             dataType: "text",
                             type: "POST",
                             url: "ajax/insert_warning.php"
                           })

                         // In case of succesfull connection with the server
                         .done(function(data) {

                            //If correct insert, alert it and close the student card.
                            if(data == "Warning inserted correctly.") {
                              createDialog(data);
                              $("#"+dialogo_id).dialog( "close" );
                            } else {

                                //If database error during the transaction, show it.
                                createDialog(data);
                              }
                            })

                     // In case of connection error, warn the user
                     .error(function(){
                      createDialog("Database connection error.\nPlease try again later.");
                    });
                   }
                 }
               },
               {
                text: "Cancel",
                id: "button_cancel",
                click: function() {

                    // Checks different cases of warning input in order to cancel the warning
                    if((points.value > 0) || (char_counter.innerHTML > 0)) {

                        // If user wants to leave without saving the warning, close the dialog.
                        createConfirm($("#"+dialogo_id),"You have setup a warning.\nAre you sure you want to leave without save it? "); 
                      } else {

					// If there is no input (no points & no description, close modalbox)
					$("#"+dialogo_id).dialog( "close" );
				}
			}
		}
		]

	});
});
});
</script>

<?php 

// Get students list
require 'requires/get_student_list.php'; 
?>
</div>