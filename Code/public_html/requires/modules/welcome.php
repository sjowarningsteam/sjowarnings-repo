<!-- Author: 	Daniel Catalán -->
<!-- Date: 		1200114 -->
<!-- File: 		welcome.php -->

<style type="text/css">

	.resaltado {
		font-weight: bold;
	}

	.welcome {
		padding:0px;
		margin-left: 10px;
	}

	#welcome_teacher {
		border: 0;
		float: left;
		font-size: 18px;
		/*display: inline-block;*/
		text-align: center;
		margin: 50px 60px;
	}

	#warning_info {
		border: 1px solid black;
		border-radius: 12px;
		font-size: 16px;
		height: 180px;
		margin-bottom: 10px;
		margin-bottom: 20px;
		margin-left: 575px;
		margin-top: 20px;
		text-align: center;
		width: 150px;
	}

	#teacher_info {
		border: 2px solid #a90023;
	}

	h3 {
		background: #a90023;
		border-radius: 12px;
		color: white;
		font-size: 15px;
		font-weight: bold;
		line-height: 40px;
		margin-bottom: 20px;
		margin: -1px;
		text-align: center;
}

.points { 
	margin-top:20px;
}

</style>
<div id="teacher_info">
	<div id="welcome_teacher">
		<?php
		$table = strtolower($_SESSION['name_type']);
		
		// Get user info from database
		$query = "select name_".$table.", lastname_".$table." from ".$table." where id_user='".$_SESSION['id_user']."'";
		$result = @mysqli_query($dbc,$query);
		if($row = mysqli_fetch_array($result, MYSQL_ASSOC))
		{
			$name = $row['name_'.$table.''];
			$lastname = $row['lastname_'.$table.''];
		}
		
		// Show welcome message personalized
		echo '<p class="welcome">Welcome, ' . $table . ' ' .$name.' '. $lastname.'.</p>';
		$date = date('l, F \t\h\e d\t\h \o\f Y');
		echo '<p class="welcome">Today is '. $date.'.</p>';
	?>
		
	</div>
	<div id='warning_info' class="warning_info">
		
		<?php
		echo '<h3 class="warning_info">Summary</h3>';
		
		if($table != "student"){
			//Declare queries
			$quantitywarning = "select count(id_warning) as countwarn from warning";
			$quantitystudent = "select count(id_student) as countstud from student";
			
			$result2 = @mysqli_query($dbc,$quantitywarning);
			if($row = mysqli_fetch_array($result2, MYSQL_ASSOC))
			{
				$int_warning = $row['countwarn'];
			}
			
			$result3 = @mysqli_query($dbc,$quantitystudent);
			if($row = mysqli_fetch_array($result3, MYSQL_ASSOC))
			{
				$int_student = $row['countstud'];
			}

			echo '<br>There are <span class="resaltado">'. $int_warning.'</span> Warnings<br><br>';
			echo 'There are <span class="resaltado">'. $int_student.'</span> Students';
		}
		//If the user log on as student
		else {
			
			$id_student = $_SESSION['id_student'];
			//query
			$quantity_warning_point = "SELECT sum(points_quantity_warning) as countpoint from warning where id_warning_status = 2 and id_student =".$id_student;
			
			$result4 = @mysqli_query($dbc,$quantity_warning_point);
			if($row = mysqli_fetch_array($result4, MYSQL_ASSOC))
			{
				$warning_student_point = $row['countpoint'];
			}
			//In case we didn't find any warning, it shows 0 in message.
			if ($warning_student_point == null) {
				$warning_student_point = 0;
			}
			echo '<p>Currently, you have <span class="resaltado">'. $warning_student_point.'</span> Warning Points acumulated. </p>';
		}
	?>

	</div>
</div>