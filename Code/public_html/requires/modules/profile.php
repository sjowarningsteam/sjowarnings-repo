<!--
Name: Vitaly
Date: 18/01/2015	
-->
<style type="text/css">

	#profile {
		border: 3px solid #960018;
		padding: 10px 0 0 10px;
		text-align: center;
	}

	#info_teacher {
		border:none;
		text-align: justify;
	}

	#new_values {
		height: 100px;
		width: 400px;
	}

	#mail_link {
		color: white;
		text-decoration: none;
	}

	#button_mail {
		background-color: #960018;
		border-radius: 5px;
		border: 1px solid;
		margin-bottom: 5px;
	}

</style>

<?php

#Get user info from database
$query = "SELECT name_".$table.", lastname_".$table.", phone_".$table.", email_".$table." from ".$table." where id_user = '".$_SESSION['id_user']."'";
$result = @mysqli_query($dbc,$query);
if ($row = mysqli_fetch_array($result,MYSQL_ASSOC)) {
	$name = $row['name_'.$table.''];
	$lastname = $row['lastname_'.$table.''];
	$phone = $row['phone_'.$table.''];
	$email = $row['email_'.$table.''];
}
?>

<div id="profile">
	<div id="info_teacher">
		<?php
		
		#Show all information about teacher
		echo "<strong>Fullname:</strong> ".$name. " ". $lastname."<br>";
		echo "<strong> Phone contact:</strong> ".$phone."<br>";
		echo "<strong>Email:</strong> ".$email."<br>";
		?>
	</div>

	<!-- Button for send an mail for edit personal information -->
	<p><strong> Do you want change your information? Click to the next button:</strong></p>
	<button id="button_mail"><a href="mailto:info@sjowarnings.com?subject=Modify%20information" id="mail_link">Send Mail</a></button>
</div>	