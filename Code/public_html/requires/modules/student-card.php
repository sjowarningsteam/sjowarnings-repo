<div id='student-card'>
	<link rel='stylesheet' type='text/css' href='../../css/student-card.css'>
	<script type='text/javascript' src='../../js/student-card.js'></script>
	<div id='sc-photo'>
		<img src='../../img/rabbit_avatar.png'>
	</div>
	<div id='sc-personal-info'>
		<ul>
			<li>Name: </li>
			<li>Lastname: </li>
			<li>Current points: </li>
		</ul>
		<div id='set-points'>
			<button id='button_plus'>+</button><input id='points' type='text' value='0' disabled><button id='button_minus'>-</button>
		</div>
	</div>
	<div id='sc-description'>
		<textarea placeholder='Description' id='description' maxlength='590'></textarea>
		<p id='chars_written'>Characters written: <span id='char_counter'>0</span> of 590</p>
	</div>
	<div id='sc-bottom'>
		<form method='POST' action=''>
			<input id='button_confirm' type='submit' value='Confirm'><input id='button_cancel' type='button' value='Cancel'>
		</form>
	</div>
</div>