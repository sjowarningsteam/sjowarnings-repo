<!-- Author: 	Daniel Catalán -->
<!-- Date: 		2150119 -->
<!-- File: 		headmaster-warning-list.php -->

<div id="headmaster-warning">
	<link rel="stylesheet" href="css/headmaster-warning-list.css">
	<input type="button" value="Save changes" class="save_button">

	<div id="table_warnings_headmaster">
		<?php require 'ajax/get_warnings_list_headmaster.php'; ?>
	</div>
	<input type="button" value="Save changes" class="save_button">
	
	<script>
	//when the page is charged
	window.onload=function() {

		function ajax_refresh(){
		// Refresh the table with data updated.
		var result = "";
		$.ajax({
			data: result,
			type: "html",
			type: "POST",
			url: "ajax/get_warnings_list_headmaster.php"
		})
		.done(function(result) {
			// console.log(result);
			$('#table_warnings_headmaster').html(result);
			var array_accepted = [];
			var array_refused = [];
		});
	};

	function ajax_update(){
	// Save all the modified id's
	var data = {
		accepted: array_accepted,
		refused: array_refused
	};

	$.ajax({
		data: data,
		dataType: "text",
		type: "POST",
		url: "ajax/update_warnings_status.php"
	})
	.done(function(data) {
		console.log("ajax done: "+data);
		ajax_refresh();
	});
}

$( ".save_button" ).click( function(){ 

		// Check if there is any warning status modified
		if ( ( array_accepted.length > 0 ) || ( array_refused.length > 0 ) ) {
			
			// If there is any change, do the ajax request
			ajax_update();

		}
	});


	//Loop over array searching element
	function searchElement(value,array) {
		for (var i = array.length - 1; i >= 0; i--) {
			if ( array[i] === value ) {
				return true;
			}
		};
		return false;
	}
	// Check if given element exists in given array.
	function checkIfExists(value,array_one,array_two){

    	// Check if exists in the first array
    	if ( !searchElement(value, array_one) ) {
    		array_one.push(value);
    	}

    	// Check if exists in the second array.
    	if ( searchElement(value, array_two) ) {
    		array_two.splice(array_two.indexOf(value),1);
    	}
    }

	//Arrays for save the warnings id's
	var array_accepted = [];
	var array_refused = [];

	//Click on accepted button it will be colored green
	$('.v').click(function(){
		var rowSelected = $(this).parent().parent().parent();
		var id_warning_v = rowSelected.attr('id');
		checkIfExists(id_warning_v, array_accepted, array_refused);
		rowSelected.css("background-color", "green");
	});

    //Click on refused button it will be colored red
    $('.x').click(function(){
    	var rowSelected = $(this).parent().parent().parent();
    	var id_warning_x = rowSelected.attr('id');
    	checkIfExists(id_warning_x, array_refused, array_accepted);
    	rowSelected.css("background-color", "red");
    });

}

</script>
</div>