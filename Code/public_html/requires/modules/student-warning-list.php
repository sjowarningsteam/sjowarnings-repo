<!-- Author: 	Daniel Catalán -->
<!-- Date: 		2150119 -->
<!-- File: 		student-warning-list.php -->
<div id="students-list">
	<link rel="stylesheet" type="text/css" href="css/students-list.css">
	<?php 
	$id_user = $_SESSION['id_user'];
	// Database connection
	// require 'requires/require_mysqli_connect.php';
	// Get user info from database
	$query = "SELECT DATE_FORMAT(date_warning,'%Y-%m-%d %H:%i'), w.points_quantity_warning, description, concat_ws(', ',LASTNAME_STUDENT, NAME_STUDENT) as Student, concat_ws(', ' ,lastname_teacher, name_teacher) as Teacher, 
	concat_ws(', ',lastname_headmaster, Name_HEADMASTER) as Headmaster FROM warning as w inner join student as s on w.id_student = s.id_student inner join teacher as t on w.id_teacher = t.id_teacher inner join headmaster as h on w.id_headmaster = h.id_headmaster WHERE s.id_user = '$id_user' AND id_warning_status = 2";
	$result = @mysqli_query($dbc,$query);
	// Table header.
	echo '<div id="table_students"><table>';
	echo '<tr><th>Date</th><th>Points</th><th>Description</th><th>Student</th><th>Teacher</th><th>Headmaster</th></tr>';

	// Fetch and print all the records:
	while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
		echo '<tr>';
		foreach ($row as $key => $value) {
			echo "<td align='center'>".$value."</td>";
		}
		echo '</tr>';
	}
	// Close the table.
	echo '</table></div>'; 
	// Free up the resources
	mysqli_free_result ($result);
	?>
	<script>
		$("tr").not(':first').hover(
			function () {
				$(this).css("background","#ffff66");
			}, 
			function () {
				$(this).css("background","");
			}
			);
		</script>
	</div>