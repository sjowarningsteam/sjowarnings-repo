<div id="menu-top">
	<?php
	$fileName = basename($_SERVER['PHP_SELF']);
	$type = strtolower($_SESSION['name_type']); 
	?>
	<!-- If page name corresponds with the nav menú element name, change this element class to 'highlitgh' -->
	<a href="./welcome.php" <?php if($fileName=='welcome.php') echo "class='menu-hightlight'"; ?>>
		Main page
	</a> |
	
	<?php switch ($type) {
		case 'teacher':
		?>
		<a href="./student-list.php" <?php if($fileName=='student-list.php') echo "class='menu-hightlight'"; ?>>
			Students list
		</a> 
		<?php 
		break;
		case 'student':
		?> 
		<a href="./student-warning-list.php" <?php if($fileName=='student-warning-list.php') echo "class='menu-hightlight'"; ?>>
			Warnings list
		</a> 
		<?php 
		break;
		
		case 'headmaster':
		?>
		<a href="./headmaster-warning-list.php" <?php if($fileName=='headmaster-warning-list.php') echo "class='menu-hightlight'"; ?>>
			Warnings list
		</a>
		<?php 
		break;
	} ?>
	|

	<a href="./profile.php" <?php if($fileName=='profile.php') echo "class='menu-hightlight'"; ?>>
		Profile
	</a> 
</div>